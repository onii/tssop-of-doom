# SSOP-28 OF DOOM

![PCB image](preview.svg)

This is an intentionally difficult layout to practice toner transfer
techniques. Uses 6 mil traces and an SSOP-28 IC broken out to big pads.

The new megaAVR 0-series (ex: 3208) uses SSOP-28.
The new ATTiny 0 and 1-series (ex: 3216) uses SOIC-20.

Comparison:

| --- | megaAVR | megaAVR | ATTiny |
|--- | --- | --- | --- |
|**Model** | 4808 | 3208 | 3216 |
|**I/O pins** | 23 | 23 |  18 |
|**Flash** | 48KB | 32KB | 32KB |
|**SRAM** | 6k | 4k | 2k |
|**Pin pitch** | .65 | .65 | 1.27 |
|**Price** | $1.15 | $1.03 | $1.00 |

$.03 buys you 5 extra pins and twice the SRAM.

$.15 buys you 5 extra pins, +16KB flash and 3x the SRAM.

So obviously the 4808 is a better value if you can deal with the pin pitch.
